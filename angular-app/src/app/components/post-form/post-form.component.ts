import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { PostsService } from "../../services/posts.service";
import { Post } from "../../models/Post"

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
//PROPERTIES

  //defining our outPut as 'newPost', setting to an EventEmitter, which will have the of post and assigning it as a new EventEmitter.
  @Output() newPost: EventEmitter<Post> = new EventEmitter();
  @Output() updatedPost : EventEmitter<Post> = new EventEmitter();
  @Input() currentPost: Post;
  @Input() isEdit: boolean;

  //injection service as a dependency
  constructor(private postsService: PostsService) { }

  ngOnInit(): void {
  }

  addPost(title, body){
    console.log(title, body)
    if (!title || !body) {
      alert('Please enter a post')
    } else {
      // console.log(title, body);

      this.postsService.savePost({title, body} as Post).subscribe(post =>{
        // console.log(posts);
        this.newPost.emit(post);
      })
    }
  }

  updatePost() {
    // console.log("Updating post...");
    this.postsService.updatePost(this.currentPost).subscribe(post => {
      console.log(post);
      this.isEdit = false;
      this.updatedPost.emit(post)
    })
  }




} //end of class don't delete!
