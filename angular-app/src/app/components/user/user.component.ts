import {Component} from "@angular/core";
import { User } from "../../models/User";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent {
//  PROPERTIES- like an attribute of the Component
   firstName = 'Clark'; //ending with semicolon not a comma
   lastName = 'Kent';
   age = 65;


  //MORE PRACTICAL WAY OF CREATING PROPERTIES
  //SYNTAX : propertyName : dataType
  // firstName: string;
  // lastName: string;
  // age: number;
  // whatever: any;
  // hasKids: boolean;



  //DATA-TYPES
  // numberArray: number[]; //this MUST be an array of numbers
  //
  // stringArray: string[]; //this MUST ben an array of strings
  //
  // mixArray: any[]; //this can be an array of anything but IT HAS TO BE AN ARRAY

  //MIGHT SEE THIS MORE OFTEN
  // user: User;

//  METHODS - a 'function' inside of a class

// CONSTRUCTOR - runs every time when our component is initialized / called
//   constructor() {
    // console.log('Hello from user component!');
    //
    // this.greeting();
    // console.log(this.age); //undefined
    // this.hasBirthday();
    // console.log(this.age); //31

    // this.firstName = 'Clark';
    // this.lastName = 'Kent';
    // this.age = 65;
    // this.hasKids = false;
    // this.greeting();

    // this.numberArray = [12, 33, 24];
    // this.stringArray = ['12' , '22' , '44'];
    // this.mixArray = [12, 'stephen', true];


    //CONNECTING INTO OUR INTERFACE
  //   this.user = {
  //     firstName: 'Jane',
  //     lastName: 'Doe',
  //     age: 30,
  //     address: {
  //       street: '150 Alamo Plaza',
  //       city: 'San Antonio',
  //       state: 'TX'
  //     }
  //   }
  // }
  //
  // greeting(){
  //   // console.log('Hello there, ' + this.firstName + ' ' + this.lastName);
  //   return `Hello there, ${this.user.firstName} ${this.user.lastName}`;
  //
  //
  // }
  // hasBirthday(){
  //   this.user.age = 30;
  //   return this.user.age += 1;
  // }

} //END OF CLASS DON'T DELETE!
