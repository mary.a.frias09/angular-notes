import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  template:  `<h1>Hello</h1>`,
  styleUrls: ['./math.component.css']
})

export class MathComponent implements OnInit {
  //In your math.component.ts, create two properties, 'num1' and 'num2'.
  //Set both properties as a number.
   num1 : number;
   num2 : number;


  constructor() {

    // this.addNumbers(12,2);
    // this.subtract(12,2);
    // this.multiple(12,2);
    // console.log(this.divide(30,2));
    this.table();
  }
  ngOnInit(): void {
  }
//Next, create a method that takes in the two properties as inputs and
//returns the sum of the two properties.
    addNumbers(x,y){
      this.num1 = x;
      this.num2 = y;
      return`Addition: ${this.num1} + ${this.num2} = ${this.num1 + this.num2}`;

    }

//Create a method that takes in the two properties and returns the difference of the two numbers.
//Console log the result
    subtract(a,b){
      this.num1 = a;
      this.num2 = b;
      console.log(this.num1 - this.num2);
    }

// Create a method that takes in the two properties and
// returns the product of the two numbers.

    multiple(a,b){
      this.num1 = a;
      this.num2 = b;
      console.log(this.num1 * this.num2);
      }

// Create a method that takes in the two properties and returns the quotient of the two numbers.
// Console log the result

  divide(a,b){
    this.num1 = a;
    this.num2 = b;
    return this.num1 / this.num2;
    }







//FIZZBUZZ
// BONUS: Write a program that prints the numbers from 1 to 100. But for multiples of three,
// print 'Fizz' instead of the number. For the multiples of five point, print 'Buzz'.
// And for the numbers which are mulitples of both three and five, print 'FizzBuzz'

  table(){
    for (var i = 1; i < 100; i++){
      if (i % 3 === 0 && i % 15 !== 0) {
        console.log('Fizz');
      } else if (i % 5 === 0 && i % 15 !== 0) {
        console.log('Buzz');
      } else if (i % 15 === 0) {
        console.log('FizzBuzz');
      }else {
        console.log(i);
      }
    }
  }




} //END OF CLASS DON'T DELETE!
