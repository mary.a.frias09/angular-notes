import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from "../../models/User";
import {valueReferenceToExpression} from "@angular/compiler-cli/src/ngtsc/annotations/src/util";
import { DataService} from "../../services/data.service";
import {Data} from "@angular/router";


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  //PROPERTIES
  user: User ={
    email: "",
    age: null,
    balance: 0,
    firstName: "",
    hide: false,
    image: "",
    isActive: false,
    lastName: "",
    // memberSince: undefined,
    showUserForm: false,
  };


  users: User[];  //'Users' is like setting a variable
  // myNumberArray: number[],
  displayInfo: boolean = true;
  showUserForm: boolean = false;
  enableAddUser: boolean;
  currentClass: {};  //basically an empty object
  currentStyle: {};
  @ViewChild("userForm")form: any


  //create a property for the observable
  data: any;


  //inject the service as a dependency in our constructor
  constructor(private dataService: DataService) { }

  ngOnInit(): void {

    // this.users = this.dataService.getUsers();
    this.dataService.getUsers().subscribe(users =>{
      this.users = users;
    });
    //subscribe to the observable
    // this.dataService.getData().subscribe(data =>{
    //   console.log(data)
    // });


    this.enableAddUser = false;

    // this.setCurrentClasses();

    // this.setCurrentStyle();


    // Create a method that will change the state of our button
    // setCurrentClasses(){
    //   this.currentClass = {
    //     'btn-dark': this.enableAddUser,
    //   }
    // };
    // };

    //Create a method that will add padding to the users name
    // when the users info is not displaying (this.displayInfo = false)
//   setCurrentStyle(){
//     this.currentStyle = {
//       'padding-top': this.displayInfo ? '0' : '80px'
//     }
//  }

    //adding in fireEvent method under click event
    //TYPE OF EVENTS-
    /*(mousedown), (mouseup), (mouseout), (click), (dblclick), (drag)  */

    // fireEvent(e){
    //  console.log('The event has just happened');
    //   // console.log(e.type);
    //   // console.log("The dragover has been activated");
    // }


  }  //ngOnInit() closing bracket



  onSubmit({ value,valid }:{ value:User, valid:boolean }){
    if(!valid){
      // alert("Your Form is invalid!!!!!")
    }else{
      value.isActive = true
      value.memberSince = new Date();
      value.hide = true;

      //Faiths code from forms
      // this.users.unshift(value);

      //Stevens code from services
      this.dataService.addUser(value);

      //resets the form
      this.form.reset();
    }
  }


  toggleHide(user: User){
    user.hide = !user.hide;
  }
}  //End of class don't delete!!
