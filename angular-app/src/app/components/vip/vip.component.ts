import { Component, OnInit } from '@angular/core';
import { Member } from "../../models/Member";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.component.html',
  styleUrls: ['./vip.component.css']
})
export class VipComponent implements OnInit {
  member: Member [];
  message: any;
  loadingMembers: boolean = false;


  constructor() {
  }

  ngOnInit(): void {
    setTimeout(()=> {this.loadingMembers = true},5000)
  this.member = [
    {
      firstName: 'MaryAnn',
      lastName: 'Frias',
      username: 'ImMaryAnnF',
      memberNo: 39
  },
    {
      firstName: 'Joe',
      lastName: 'Mazz',
      username: 'ImJoeM',
      memberNo: 42
  },
    {
      firstName: 'Erika',
      lastName: 'Herrera',
      username: 'ImErikaH',
      memberNo: 31
  },
    {
      firstName: 'Patricia',
      lastName: 'Valencia',
      username: 'ImPatriciaV',
      memberNo: 32
  },
    {
      firstName: 'Mallari',
      lastName: 'Flores',
      username: 'ImMallariF',
      memberNo: 19
    }
  ]}; //sets the array


}//END OF CLASS DON'T DELETE!!
