import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Post } from "../models/Post";



const httpOptions = {
  headers: new HttpHeaders({'Content-Type' : 'application/json'})
}

@Injectable({
  providedIn: 'root'
})


export class PostsService {
  //Properties
  //set a url as a property
  postURL: string = 'https://jsonplaceholder.typicode.com/posts';


  //inject our httpclient as a dependency inside our constructor
  constructor(private http: HttpClient) { }


  //create a method, that will make a GET request
  // at the end of the class!!

  getPosts() : Observable<Post[]> {
    return this.http.get<Post[]>(this.postURL)
  }


  //create a method savePost()
  savePost(post: Post) : Observable<Post> {
    return this.http.post<Post>(this.postURL, post, httpOptions)
  }



  //create the updatePost method
  updatePost(post: Post) : Observable<Post> {
    const url = `${this.postURL}/${post.id}`  //url'https://jsonplaceholder.typicode.com/posts' update post.id

    return this.http.put<Post>(url, post, httpOptions)
  }

  removePost(post: Post | number) : Observable<Post> {
    const id = typeof post === 'number' ? post : post.id;

    const url =  `${this.postURL}/${id}`;

    console.log('Deleting post...');
    alert('Post removed');
    
    return this.http.delete<Post>(url, httpOptions)


  }



}//end of class, don't delete

