import { Injectable } from '@angular/core';
import { User } from "../models/User";
import {Observable , of } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class DataService {
  //Initialize a user's property and assign it to an array of User
  users: User[]

  data:Observable <any>

  constructor() {
    this.users = [  //adding the square bracket makes it an array
      {
        firstName: 'Bruce',
        lastName: 'Wayne',
        age: 30,
        email: 'bruce@gmail.com',
        image:'../assets/image/bruce-wayne.jpg',
        hide: true,
        balance: 1000000,
        memberSince: new Date('03/01/1937 08:30:00')
      },
      {
        firstName: 'Diana',
        lastName: 'Prince',
        age: 110,
        email: 'diana@gmail.com',
        isActive: true,
        image:'../assets/image/diana-prince.jpg',
        hide: true,
        balance: 15000000,
        memberSince: new Date('12/12/1942 08:30:00')
      }

    ]; //semicolon sets the array
  } //end of constructor


  //get data example
  // getData(){
  //   this.data = new Observable(observer => {
  //     setTimeout(()=>{
  //       observer.next(1)
  //     }, 1000);
  //
  //     setTimeout(()=>{
  //       observer.next(2)
  //     }, 2000);
  //
  //     setTimeout(()=>{
  //       observer.next(3)
  //     }, 3000);
  //
  //     setTimeout(()=>{
  //       observer.next("stephen")
  //     }, 4000);
  //   });
  //   return this.data;
  // }

  //getUsers(), return a type of User[]

  //refactor our method to return an array as an Observable
  getUsers(): Observable<User[]> {
    console.log('Fetching Users from Service.')
    return of(this.users);
  }

  // CREATE a method that adds a new user to the array
  addUser(user:User){
    console.log('Added User from Service.')
    this.users.unshift(user);
  }


}
